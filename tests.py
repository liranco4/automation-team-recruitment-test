import json
import sys
import delete_old_versions


try:
    for i in range(1, 5):
        print("Checking 'versions{0}.json'...".format(i))

        with open("tests/versions{0}.json".format(i), mode='r') as f:
            s = f.read()

        result = json.loads(delete_old_versions.delete_old_versions(s, 100))

        expected_items = []
        with open("tests/versions{0}_ids.txt".format(i), mode='r') as f:
            for l in f:
                expected_items.append(l.strip())

        for i in expected_items:
            try:
                assert(i in [x['id'] for x in result['versions']])
            except AssertionError:
                print(("Uh oh... something is wrong. We expected " +
                      "{0} to be in the result but we can't find it.".format(i)))
                sys.exit()

        try:
            assert(len(result['versions']) == len(expected_items))
        except AssertionError:
            print('Oops... looks like we have unnecessary items in our result.')
            sys.exit()

        print('Looks good.')

    print('All tests have passed. Nice job!')
except Exception as e:
    print("Hmm. Something went wrong. Maybe this will help:\n" + str(e))
